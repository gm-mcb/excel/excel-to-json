package it.gimar.fs;

import lombok.extern.java.Log;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

@Log
public class ProcessDir {

    private final Path path;
    private final String suffix = ".xlsx";

    public ProcessDir(Path path) throws NotADirectoryException {
        this.path = path;
        if(!Files.isDirectory(this.path)){
            throw new NotADirectoryException(this.path);
        }
    }
    public List<Path> getFiles() throws IOException {
        List<Path> files = Files.list(this.path)
            .collect(Collectors.toList()).stream()
            .filter(f -> {
                log.info(f.getFileName().toString());
                return f.getFileName().toString().endsWith("xlsx");
            })
            .map(f -> {
                log.info("F: " + f.toString());
                return f;
            })
            .collect(Collectors.toList());
        return files;
    }

    public class NotADirectoryException extends Throwable {
        public NotADirectoryException(Path path) {
            super(path.toString() + " is not a directory");
        }
    }
}
