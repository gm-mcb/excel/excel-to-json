package it.gimar.xlsx;

import lombok.extern.java.Log;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.nio.file.Path;

@Log
public class TestXlsxHelper {

    public static XSSFWorkbook getWorkbook(Path path) throws Exception {
        File file = path.toFile();
        byte[] bytes = new byte[0];
        try (
            FileInputStream fip = new FileInputStream(file)) {
            XSSFWorkbook workbook = new XSSFWorkbook(fip);
            try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                workbook.write(baos);
                bytes = baos.toByteArray();
            }
        } catch (
            FileNotFoundException e) {
            log.severe(e.getLocalizedMessage());
            e.printStackTrace();
        } catch (
            IOException e) {
            log.severe(e.getLocalizedMessage());
            e.printStackTrace();
        }
        if (bytes.length > 0) {
            ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
            XSSFWorkbook workbook = new XSSFWorkbook(bais);
            return workbook;
        }
        throw new Exception("No test workbook to pass on");
    }
}
