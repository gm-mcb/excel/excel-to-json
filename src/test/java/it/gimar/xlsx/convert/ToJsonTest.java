package it.gimar.xlsx.convert;

import com.jsoniter.JsonIterator;
import com.jsoniter.any.Any;
import it.gimar.xlsx.TestResourceHelper;
import it.gimar.xlsx.TestXlsxHelper;
import it.gimar.xlsx.exception.NotAValidFileException;
import it.gimar.xlsx.exception.NotAnXlsxException;
import lombok.extern.java.Log;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

@Log
public class ToJsonTest {

    Path resourcePath;
    String sheet = "Sheet1";
    private XSSFWorkbook startWb;

    @Test
    public void convertSingleSheet_Strange_Many_Sheets_Test() throws NotAnXlsxException, NotAValidFileException, Exception {
        this.resourcePath = Paths.get(TestResourceHelper.exportResource("/for-json-strange.xlsx"));
        this.startWb = TestXlsxHelper.getWorkbook(this.resourcePath);
        ToJson toJson = new ToJson(this.resourcePath);
        byte[] outBytes = toJson.convert(sheet);
        outBytes = toJson.convert();

        assertThat(outBytes.length).isGreaterThan(0);
        Any json = JsonIterator.deserialize(outBytes);
        List<XSSFSheet> sheets = IntStream.range(0, this.startWb.getNumberOfSheets())
            .mapToObj( i -> this.startWb.getSheetAt(i))
            .collect(Collectors.toList());
        for(XSSFSheet xssfSheet : sheets) {
            Any anySheet = json.asMap().get(xssfSheet.getSheetName());
            Any records = anySheet.get("records");
            assertThat(records).hasSize(this.startWb.getSheet(sheet).getLastRowNum());
            for (Any record : records.asList()) {
                String key = record.toString("key");
                assertThat(key).isNotNull();
                Any languages = record.get("values");
                assertThat(languages).hasSize(4);
                for (Any language : languages.asList()) {
                    assertThat(language.toString("name")).isNotEmpty();
                    assertThat(language.toString("value")).isNotNull();
                }
            }
        }

    }
    @Test
    public void convertSingleSheet_Strange_Single_Sheet_Test() throws NotAnXlsxException, NotAValidFileException, Exception {
        this.resourcePath = Paths.get(TestResourceHelper.exportResource("/for-json-strange.xlsx"));
        this.startWb = TestXlsxHelper.getWorkbook(this.resourcePath);
        ToJson toJson = new ToJson(this.resourcePath);
        byte[] outBytes = toJson.convert(sheet);

        assertThat(outBytes.length).isGreaterThan(0);

        Any json = JsonIterator.deserialize(outBytes);
        Any records = json.asMap().get("records");
        assertThat(records).hasSize(this.startWb.getSheet(sheet).getLastRowNum());
        for(Any record : records.asList()){
            String key = record.toString("key");
            assertThat(key).isNotNull();
            Any languages = record.get("values");
            assertThat(languages).hasSize(4);
            for(Any language : languages.asList()){
                assertThat(language.toString("name")).isNotEmpty();
                assertThat(language.toString("value")).isNotNull();
            }
        }
    }
    @Test
    public void convertWithNumeric_Test() throws NotAnXlsxException, NotAValidFileException, Exception {
        this.resourcePath = Paths.get(TestResourceHelper.exportResource("/for-json-numeric.xlsx"));
        this.startWb = TestXlsxHelper.getWorkbook(this.resourcePath);
        ToJson toJson = new ToJson(this.resourcePath);
        byte[] outBytes = toJson.convert(sheet);

        assertThat(outBytes.length).isGreaterThan(0);

        Any json = JsonIterator.deserialize(outBytes);
        Any records = json.asMap().get("records");
        assertThat(records).hasSize(1);
        Any record = records.get(0);
        String key = record.toString("key");
        assertThat(key).isEqualTo("nums");
        Any values = record.get("values");
        assertThat(values).hasSize(4);
        Any textNumberValue0 = values.get(0).get("value");
        assertThat(textNumberValue0.toString()).isEqualTo("4");
        Any textNumberValue1 = values.get(1).get("value");
        assertThat(textNumberValue1.toString()).isEqualTo("4.5");
        Any textNumberValue2 = values.get(2).get("value");
        assertThat(textNumberValue2.toString()).isEqualTo("500");
        Any textNumberValueDate = values.get(3).get("value");
        assertThat(textNumberValueDate.toString()).startsWith("Tuesday\", \"April");
    }
}
