#!/bin/bash

SWITCH="\e["
NORMAL="${SWITCH}0m"
YELLOW="${SWITCH}33m"
RED="${SWITCH}31m"
GREEN="${SWITCH}32m"
BLUE="${SWITCH}34m"

exe="${BASH_SOURCE[0]}"
relative_dir=$(dirname "${exe}")
dir=$(cd ${relative_dir}; pwd)

pushd "$dir" 2>&1 > /dev/null

java -jar excel-to-json.jar -f sample-json/for-json.xlsx -s Sheet1 -j output/translate-db.json
err=$?

popd 2>&1 > /dev/null
exit $err
